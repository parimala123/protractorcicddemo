const { browser, element } = require('protractor')

describe('Chain Locators demo', function(){
    //Writing reusable functions
    function Add(a,b){
        element(by.model('first')).sendKeys(a)
        element(by.model('second')).sendKeys(b)
        element(by.id('gobutton')).click() 
    }

     it('Gettting Text from chain Locators', function(){

         browser.get('https://juliemr.github.io/protractor-demo/')

         element(by.model('operator')).element(by.css('option:nth-child(3)')).getText().then(function(text){
             console.log(text)
         })

         Add(2,3)
         Add(1,1)
         Add(5,3)

        //chaining the locators
        //  element(by.repeater('result in memory')).element(by.css('td:nth-child(3)')).getText().then(function(text){
        //          console.log(text)
        //      })

        element.all(by.repeater('result in memory')).count().then(function(text){
            console.log(text)
        })
        // fetching data from a webtable 
        element.all(by.repeater('result in memory')).each(function(item){
            //to get the single column value in each row
            // item.element(by.css('td:nth-child(3)')).getText().then(function(text){
            //     console.log(text)
            // })
            item.all(by.xpath("td")).each(function(col){
                col.getText().then(function(text){
                    console.log(text)
                })
            })
        })
     })
})

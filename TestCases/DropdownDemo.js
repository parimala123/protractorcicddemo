const { browser, element } = require('protractor')

describe('Chain Locators demo', function(){
    //Writing reusable functions
    function Calc(a,b,OperatioType){
        element(by.model('first')).sendKeys(a)
        element(by.model('second')).sendKeys(b)
        //Handling the dropdown
        element.all(by.tagName('option')).each(function(item){
            item.getAttribute('value').then(function(OptionValue){
                if(OptionValue === OperatioType) {
                    item.click()
                }
            })
        })
        element(by.id('gobutton')).click() 
    }

     it('Gettting Text from chain Locators', function(){

         browser.get('https://juliemr.github.io/protractor-demo/')

         Calc(4,4,'ADDITION')
         Calc(3,3,'DIVISION')
         Calc(7,7,'MULTIPLICATION')
         
        // fetching data from a webtable 
        element.all(by.repeater('result in memory')).each(function(item){
            item.all(by.xpath("td")).each(function(col){
                col.getText().then(function(text){
                    console.log(text)
                })
            })
        })
     })
})

const { browser } = require("protractor");

describe('Actions demo', function () {

    it('Open Posse website', function () {
        //moving the mouse into that textbox

        browser.get("https://posse.com/");
        element(by.model("userInputQuery")).sendKeys("river");
        browser.actions().mouseMove(element(by.model("locationQuery")).sendKeys("london")).perform();

        browser.actions().sendKeys(protractor.Key.ARROW_DOWN).perform();
        browser.actions().sendKeys(protractor.Key.ENTER).perform().then(function () {

            browser.sleep(2000);

            expect(element.all(by.css("div[ng-mouseover*='onSearchResultOver']")).count()).toEqual(7);


            element.all(by.css("div[ng-mouseover*='onSearchResultOver']")).get(0).click();
            //clicking on the first link in the search results
            element(by.css("a[ng-href*='London/River Island']")).click().then(function () {
                browser.sleep(2000);
                browser.getAllWindowHandles().then(function (handles) {
                    //0th index is parent so to switch to chaild window we have used 1st index
                    browser.switchTo().window(handles[1])
                    browser.getTitle().then(function (title) {
                        console.log(title + " After switching to the child window")
                    })
                    //switching back to the child window
                    browser.switchTo().window(handles[0])
                })
            })

        })

    })
})


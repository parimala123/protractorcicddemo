const { browser, element } = require("protractor");

describe('Protractor Sync steps', function () {

    it('Open NonAngular Sync issues', function () {

        browser.waitForAngularEnabled(false);

        browser.driver.manage().window().maximize();

        browser.get("https://www.itgeared.com/demo/1506-ajax-loading.html");
        browser.sleep(5000);
        //element(by.css("a[href*='loadAjax']")).click()
        //or we can use the css like below
        $("a[href*='loadAjax']").click()
        var EC = protractor.ExpectedConditions;
        browser.wait(EC.invisibilityOf(element(by.id('loader'))), 8000);
        element(by.id("results")).getText().then(function (text) {
            browser.sleep(5000)
            console.log(text);
        })
    })
})

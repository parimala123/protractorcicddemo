const { browser, element } = require('protractor')

describe('Protractor element demo', function(){
     it('Locators', function(){
         browser.get('https://juliemr.github.io/protractor-demo/')
         element(by.model('first')).sendKeys(3)
         element(by.model('second')).sendKeys(5)
         element(by.id('gobutton')).click() 

         element(by.model('first')).sendKeys(1)
         element(by.model('second')).sendKeys(1)
         element(by.id('gobutton')).click()
         
         element(by.model('first')).sendKeys(2)
         element(by.model('second')).sendKeys(2)
         element(by.id('gobutton')).click() 

         //Protractor will not resolve the promise automatically to get the text
         element(by.css("h2[class='ng-binding']")).getText().then(function(text){
             console.log(text)
         });
         //Jasmine takes care of resolving the promise
         //expect(element(by.css("h2[class='ng-binding']")).getText()).toBe('8')

         //const names = element.all(by.repeater('result in memory'))
        
        //  names.count().then(function(count){
        //     let rowFound = false;
        //      console.log("rows count is ::   "+count)
        //      names.row(1).column(Result).getText().then(function(text){
        //          console.log(text)
        //      })
        //  })

        //element.all(by.repeater('result in memory'))

        // element.all(by.repeater="result in memory").each(function(row,rIndex){
        //     //do reapet action on earch row of a table 
        //      row.all(by.repeater="td").each(function(col,cIndex){
        //     //get the each column text value
        //       col.getText().then(function(){
        //        console.log("Row Index -"+rIndex+"\n Column Index -"+cIndex+"\n Col valu is-"+cValue);
        //       });
        //      });
        //    });

    //    let rows= element.all(by.repeater="result in memory")
    //    expect(rows.count()).toBe(3)
    
     })
})

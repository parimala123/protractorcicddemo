const { browser, element, by} = require('protractor')
describe('Protractor Typescript Demo', function() {
	//browser.ignoreSynchronization = true; // for non-angular websites
	browser.manage().window().maximize()
	it('Mouse Operations', function() {
		// set implicit time to 30 seconds
		//browser.manage().timeouts().implicitlyWait(30000);
        browser.waitForAngularEnabled(false)
        browser.get("http://the-internet.herokuapp.com/hovers")
		// mouse hover on a submenu
        browser.actions().mouseMove(element(by.xpath("//div[@id='content']/div/div[1]"))).perform()
        browser.sleep(2000)
	});
});
